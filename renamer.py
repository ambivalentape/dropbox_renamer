import exifread
import os
import sys
import re
dir_name = sys.argv[1]
print dir_name

regex = re.compile('[0-9]{8}\_[0-9]{6}\.')
DATE_TAG ='EXIF DateTimeOriginal'
EXTENSIONS=['jpg','jpeg']
from dropbox import status,start,running,DropboxCommand,closing

if not running([]):
    start([])

with closing(DropboxCommand()) as dc:
    try:
        lines = dc.get_dropbox_status()[u'status']
        print lines
        if lines[0] !=u'Up to date':
            sys.exit(0)

    except KeyError:
        print(u"Couldn't get status: daemon isn't responding")
        sys.exit(0)
    except DropboxCommand.CommandError, e:
        print(u"Couldn't get status: " + str(e))
        sys.exit(0)
    except DropboxCommand.BadConnectionError, e:
        print(u"Dropbox isn't responding!")
        sys.exit(0)
    except DropboxCommand.EOFError:
        print(u"Dropbox daemon stopped.")
        sys.exit(0)


for (base_folder,sub_folders,file_names) in os.walk(dir_name):
    for file_name in file_names:
        print file_name
        extension = file_name.split('.')[1]
        groups =regex.match(file_name)
        if extension.lower() not in EXTENSIONS or groups:
            if not groups:
                print 'Skipping %s' % (file_name)
            continue
        full_name = os.path.join(base_folder,'',file_name)
        fh = open(full_name,'rb')
        tags = exifread.process_file(fh)
        if DATE_TAG in tags:
            reformatted_timestamp = tags[DATE_TAG].values.replace(':','').replace(' ','_')
            print '%s -> %s (%s)' % (file_name,reformatted_timestamp,extension)
            os.rename(full_name,os.path.join(base_folder,reformatted_timestamp+'.'+extension))









